package com.tugasakhirpapb.gujob;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.tugasakhirpapb.gujob.model.Lamaran;

public class LamaranActivity extends AppCompatActivity {

    public static final int PICKFILE_RESULT_CODE = 1;

    private TextView actionTitle;
    private EditText namaInput, tanggalLahirInput, tempatLahirInput, deskripsiDiriInput, socialMediaInput;
    private Button filePickerButton, actionButton;
    private ProgressBar progressBar;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBLamaranRef;

    private FirebaseStorage mStorage;
    private StorageReference mStorageRef;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    private Uri fileUri;
    private String filePath;

    private Lamaran currentLamaran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lamaran);

        actionTitle = findViewById(R.id.ActionTitle);
        namaInput = findViewById(R.id.NamaLengkapInput);
        tanggalLahirInput = findViewById(R.id.TanggalLahirInput);
        tempatLahirInput = findViewById(R.id.TempatLahirInput);
        deskripsiDiriInput = findViewById(R.id.DeskripsiDiriInput);
        socialMediaInput = findViewById(R.id.SocialMediaInput);
        filePickerButton = findViewById(R.id.FilePickerButton);
        actionButton = findViewById(R.id.ActionButton);
        progressBar = findViewById(R.id.progressBar);

        Intent intent = getIntent();
        boolean IsEdit = intent.getBooleanExtra("IsEdit", false);
        String perusahaanID = intent.getStringExtra("ID");
        String lamaranID = intent.getStringExtra("lamaranID");

        mDatabase = FirebaseDatabase.getInstance("https://gujob-papb-default-rtdb.asia-southeast1.firebasedatabase.app/");
        mDBLamaranRef = mDatabase.getReference("Lamaran");

        mStorage = FirebaseStorage.getInstance();
        mStorageRef = mStorage.getReference();

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        progressBar.setVisibility(View.GONE);
        if (IsEdit) {
            actionTitle.setText("Edit Lamaran");
            actionButton.setText("Simpan Perubahan");

            mDBLamaranRef.child(lamaranID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    currentLamaran = snapshot.getValue(Lamaran.class);
                    currentLamaran.setId(snapshot.getKey());
                    RefreshTampilan();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(LamaranActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            actionTitle.setText("Tambah Lamaran");
            actionButton.setText("Upload Lamaran");
            actionButton.setEnabled(false);
        }

        filePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PilihFile();
            }
        });

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionLamaran(IsEdit, perusahaanID);
            }
        });
    }

    private void RefreshTampilan() {
        if (currentLamaran == null) return;

        namaInput.setText(currentLamaran.getNama());
        tanggalLahirInput.setText(currentLamaran.getTanggalLahir());
        tempatLahirInput.setText(currentLamaran.getTempatLahir());
        deskripsiDiriInput.setText(currentLamaran.getDeskripsiDiri());
        socialMediaInput.setText(currentLamaran.getSocialMedia());
    }

    private void ActionLamaran(boolean IsEdit, String perusahaanID) {
        String nama = namaInput.getText().toString();
        String tanggalLahir = tanggalLahirInput.getText().toString();
        String tempatLahir = tempatLahirInput.getText().toString();
        String deskipsiDiri = deskripsiDiriInput.getText().toString();
        String socialMedia = socialMediaInput.getText().toString();

        actionButton.setEnabled(false);

        if (nama == null) {
            Toast.makeText(LamaranActivity.this, "Nama Lengkap Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (tanggalLahir == null) {
            Toast.makeText(LamaranActivity.this, "Tanggal Lahir Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (tempatLahir == null) {
            Toast.makeText(LamaranActivity.this, "Tempat Lahir Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (deskipsiDiri == null) {
            Toast.makeText(LamaranActivity.this, "Deskripsi Diri Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (socialMedia == null) {
            Toast.makeText(LamaranActivity.this, "Social Media Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            if (!IsEdit && filePath == null) {
                Toast.makeText(LamaranActivity.this, "CV Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
            } else {
                Lamaran newLamaran = new Lamaran();
                newLamaran.setPerusahaanID(perusahaanID);
                newLamaran.setNama(nama);
                newLamaran.setUserID(mUser.getUid());
                newLamaran.setEmail(mUser.getEmail());
                newLamaran.setTanggalLahir(tanggalLahir);
                newLamaran.setTempatLahir(tempatLahir);
                newLamaran.setDeskripsiDiri(deskipsiDiri);
                newLamaran.setSocialMedia(socialMedia);

                if (fileUri != null) {
                    StorageReference fileRef = mStorageRef.child(perusahaanID + mUser.getUid() + fileUri.getLastPathSegment());
                    UploadTask uploadTask = fileRef.putFile(fileUri);
                    progressBar.setVisibility(View.VISIBLE);
                    uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                            double progress = (100.0 * snapshot.getBytesTransferred()) / snapshot.getTotalByteCount();
                            progressBar.setProgress((int) progress);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Toast.makeText(LamaranActivity.this, "GAGAL! : " + exception.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            newLamaran.setCvUrl(taskSnapshot.getMetadata().getPath());
                            if (IsEdit) {
                                mDBLamaranRef.child(currentLamaran.getId()).setValue(newLamaran);
                                Toast.makeText(LamaranActivity.this, "Lamaran Berhasil Di Update", Toast.LENGTH_SHORT).show();
                            } else{
                                mDBLamaranRef.push().setValue(newLamaran);
                                Toast.makeText(LamaranActivity.this, "Lamaran Berhasil Di Upload", Toast.LENGTH_SHORT).show();
                            }
                            finish();
                        }
                    });
                } else {
                    mDBLamaranRef.child(currentLamaran.getId()).setValue(newLamaran);
                    Toast.makeText(LamaranActivity.this, "Lamaran Berhasil Di Update", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    }

    private void PilihFile() {
        String[] mimetypes = {
                "application/*",
        };

        Intent chooseFileActivity = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFileActivity.setType("*/*");
        chooseFileActivity.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        chooseFileActivity = Intent.createChooser(chooseFileActivity, "Choose a file");
        startActivityForResult(chooseFileActivity, PICKFILE_RESULT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == -1) {
                    fileUri = data.getData();
                    filePath = fileUri.getPath();
                    filePickerButton.setText(filePath);
                    actionButton.setEnabled(true);
                }
                break;
        }
    }
}