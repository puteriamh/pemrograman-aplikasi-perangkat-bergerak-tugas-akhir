package com.tugasakhirpapb.gujob;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tugasakhirpapb.gujob.adapter.PerusahaanAdapter;
import com.tugasakhirpapb.gujob.model.Perusahaan;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button logoutButton, addButton;
    private RecyclerView perusahaanRecyclerView;
    private PerusahaanAdapter perusahaanAdapter;

    private FirebaseAuth mAuth;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBPerusahaanRef;

    private ArrayList<Perusahaan> perusahaansList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logoutButton = findViewById(R.id.LogoutButton);
        addButton = findViewById(R.id.AddButton);
        perusahaanRecyclerView = findViewById(R.id.PerusahaanRecycleView);

        perusahaanAdapter = new PerusahaanAdapter(MainActivity.this, perusahaansList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);

        perusahaanRecyclerView.setLayoutManager(layoutManager);
        perusahaanRecyclerView.setAdapter(perusahaanAdapter);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logout();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addIntent = new Intent(MainActivity.this, TambahPerusahaanActivity.class);
                startActivity(addIntent);
            }
        });

        mAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance("https://gujob-papb-default-rtdb.asia-southeast1.firebasedatabase.app/");
        mDBPerusahaanRef = mDatabase.getReference("Perusahaan");

        mDBPerusahaanRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                perusahaansList.clear();
                for (DataSnapshot data : snapshot.getChildren()) {
                    Perusahaan newPerusahaan = data.getValue(Perusahaan.class);
                    newPerusahaan.setId(data.getKey());
                    perusahaansList.add(newPerusahaan);
                }
                perusahaanAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){
            Login();
        } else {
            logoutButton.setText(currentUser.getEmail() + " (Logout)");
        }
    }

    private void Login() {
        Intent loginActivity = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(loginActivity);
        this.finish();
    }

    private void Logout() {
        mAuth.signOut();
        Login();
    }
}