package com.tugasakhirpapb.gujob;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tugasakhirpapb.gujob.adapter.LamaranAdapter;
import com.tugasakhirpapb.gujob.adapter.PerusahaanAdapter;
import com.tugasakhirpapb.gujob.model.Lamaran;
import com.tugasakhirpapb.gujob.model.Perusahaan;

import java.util.ArrayList;

public class DetailPerusahaanActivity extends AppCompatActivity {

    private TextView namaPerusahaanView, kategoriView, deskripsiView;
    public Button actionButton, deleteButton;

    private RecyclerView lamaranRecyclerView;
    private LamaranAdapter lamaranAdapter;

    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBPerusahaanRef;
    private DatabaseReference mDBLamaranRef;


    private Perusahaan perusahaan;
    private ArrayList<Lamaran> lamaranList = new ArrayList<>();

    private Lamaran lamaranUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_perusahaan);

        Intent intent = getIntent();
        String perusahaanID = intent.getStringExtra("ID");

        namaPerusahaanView = findViewById(R.id.NamaPerusahaanTextView);
        kategoriView = findViewById(R.id.KategoriTextView);
        deskripsiView = findViewById(R.id.DeskripsiTextView);
        actionButton = findViewById(R.id.ActionButton);
        deleteButton = findViewById(R.id.DeleteButton);
        lamaranRecyclerView = findViewById(R.id.PelamarRecycleView);

        lamaranAdapter = new LamaranAdapter(DetailPerusahaanActivity.this, lamaranList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DetailPerusahaanActivity.this);

        lamaranRecyclerView.setLayoutManager(layoutManager);
        lamaranRecyclerView.setAdapter(lamaranAdapter);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance("https://gujob-papb-default-rtdb.asia-southeast1.firebasedatabase.app/");
        mDBPerusahaanRef = mDatabase.getReference("Perusahaan").child(perusahaanID);
        mDBLamaranRef = mDatabase.getReference("Lamaran");

        mDBPerusahaanRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                perusahaan = snapshot.getValue(Perusahaan.class);
                perusahaan.setId(snapshot.getKey());
                RefreshTampilan();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(DetailPerusahaanActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });

        Query lamaranQuery = mDBLamaranRef.orderByChild("perusahaanID").equalTo(perusahaanID);

        lamaranQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                lamaranList.clear();
                for (DataSnapshot data : snapshot.getChildren()) {
                    Lamaran newLamaran = data.getValue(Lamaran.class);
                    newLamaran.setId(data.getKey());

                    if (newLamaran.getUserID().equals(mAuth.getUid())) {
                        newLamaran.setNama(newLamaran.getNama() + " (Saya)");
                        lamaranUser = newLamaran;
                    }

                    lamaranList.add(newLamaran);
                }
                lamaranAdapter.notifyDataSetChanged();
                RefreshTampilan();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(DetailPerusahaanActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent actionIntent = new Intent(DetailPerusahaanActivity.this, LamaranActivity.class);
                actionIntent.putExtra("IsEdit", lamaranUser != null);
                actionIntent.putExtra("ID", perusahaanID);
                if (lamaranUser != null) {
                    actionIntent.putExtra("lamaranID", lamaranUser.getId());
                }
                startActivity(actionIntent);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lamaranUser == null) return;

                mDBLamaranRef.child(lamaranUser.getId()).removeValue();
                lamaranUser = null;
                RefreshTampilan();
            }
        });
    }

    private void RefreshTampilan() {
        if (perusahaan == null) return;

        namaPerusahaanView.setText(perusahaan.getNamaPerusahaan());
        kategoriView.setText(perusahaan.getKategori());
        deskripsiView.setText(perusahaan.getDeskripsi());

        if (lamaranUser == null) {
            actionButton.setText("Ajukan Lamaran");
            deleteButton.setVisibility(View.GONE);
        } else {
            actionButton.setText("Update Lamaran");
            deleteButton.setVisibility(View.VISIBLE);
        }
    }
}