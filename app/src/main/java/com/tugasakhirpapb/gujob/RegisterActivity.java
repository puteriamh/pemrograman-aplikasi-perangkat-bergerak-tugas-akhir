package com.tugasakhirpapb.gujob;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.tugasakhirpapb.gujob.R;

public class RegisterActivity extends AppCompatActivity {

    private EditText emailInput, passwordInput;
    private Button registerButton;
    private TextView loginTextButton;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        emailInput = findViewById(R.id.EmailTextInput);
        passwordInput = findViewById(R.id.PasswordITextnput);
        registerButton = findViewById(R.id.RegisterButton);
        loginTextButton = findViewById(R.id.LoginTextButton);

        mAuth = FirebaseAuth.getInstance();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestRegister();
            }
        });

        loginTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GoToLoginActivity();
            }
        });
    }

    private void RequestRegister() {
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();

        if (email == null) {
            Toast.makeText(RegisterActivity.this, "Email Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (password == null) {
            Toast.makeText(RegisterActivity.this, "Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(RegisterActivity.this, "Akun Berhasil Dibuat", Toast.LENGTH_SHORT).show();

                                Intent mainActivity = new Intent(RegisterActivity.this, MainActivity.class);
                                mainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                startActivity(mainActivity);
                                RegisterActivity.this.finish();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Kesalahan Dalam Membuat Akun : " + task.getException(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

    private void GoToLoginActivity() {
        //GANTI ACTIVITY KE LOGIN
        Intent registerActivity = new Intent(RegisterActivity.this, LoginActivity.class);
        registerActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(registerActivity);
        this.finish();
    }
}