package com.tugasakhirpapb.gujob.model;

public class Lamaran {
    private String id;
    private String perusahaanID;
    private String userID;
    private String nama;
    private String email;
    private String tanggalLahir;
    private String tempatLahir;
    private String deskripsiDiri;
    private String socialMedia;
    private String cvUrl;

    public Lamaran() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPerusahaanID() {
        return perusahaanID;
    }

    public void setPerusahaanID(String perusahaanID) {
        this.perusahaanID = perusahaanID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getDeskripsiDiri() {
        return deskripsiDiri;
    }

    public void setDeskripsiDiri(String deskripsiDiri) {
        this.deskripsiDiri = deskripsiDiri;
    }

    public String getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(String socialMedia) {
        this.socialMedia = socialMedia;
    }

    public String getCvUrl() {
        return cvUrl;
    }

    public void setCvUrl(String cvUrl) {
        this.cvUrl = cvUrl;
    }
}
