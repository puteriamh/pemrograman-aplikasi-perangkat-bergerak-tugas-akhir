package com.tugasakhirpapb.gujob;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tugasakhirpapb.gujob.model.Perusahaan;

public class TambahPerusahaanActivity extends AppCompatActivity {

    private EditText namaPerusahaanInput, kategoriInput, deskripsiInput;
    private Button addButton;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBPerusahaanRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_perusahaan);

        namaPerusahaanInput = findViewById(R.id.NamaPerusahaanInput);
        kategoriInput = findViewById(R.id.KategoriInput);
        deskripsiInput = findViewById(R.id.DeskripsiInput);
        addButton = findViewById(R.id.AddButton);

        mDatabase = FirebaseDatabase.getInstance("https://gujob-papb-default-rtdb.asia-southeast1.firebasedatabase.app/");
        mDBPerusahaanRef = mDatabase.getReference("Perusahaan");

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TambahPerusahaan();
            }
        });
    }

    private void TambahPerusahaan() {
        String namaPerusahaan = namaPerusahaanInput.getText().toString();
        String kategoriPerusahaan = kategoriInput.getText().toString();
        String deskripsiPerusahaan = deskripsiInput.getText().toString();

        if (namaPerusahaan == null) {
            Toast.makeText(TambahPerusahaanActivity.this, "Nama Perusahaan Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (kategoriPerusahaan == null) {
            Toast.makeText(TambahPerusahaanActivity.this, "Kategori Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (deskripsiPerusahaan == null) {
            Toast.makeText(TambahPerusahaanActivity.this, "Deskripsi Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            Perusahaan newPerusahaan = new Perusahaan();
            newPerusahaan.setNamaPerusahaan(namaPerusahaan);
            newPerusahaan.setKategori(kategoriPerusahaan);
            newPerusahaan.setDeskripsi(deskripsiPerusahaan);

            mDBPerusahaanRef.push().setValue(newPerusahaan);

            Toast.makeText(TambahPerusahaanActivity.this, "Perusahaan Berhasil Ditambahkan", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}