package com.tugasakhirpapb.gujob.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tugasakhirpapb.gujob.DetailPerusahaanActivity;
import com.tugasakhirpapb.gujob.EditPerusahaanActivity;
import com.tugasakhirpapb.gujob.R;
import com.tugasakhirpapb.gujob.model.Perusahaan;

import java.util.ArrayList;
import java.util.List;

public class PerusahaanAdapter extends RecyclerView.Adapter<PerusahaanAdapter.PerusahaanViewHolder> {

    private Context context;
    private List<Perusahaan> perusahaanList;

    public PerusahaanAdapter(Context context, ArrayList<Perusahaan> perusahaanList) {
        this.context = context;
        this.perusahaanList = perusahaanList;
    }

    @NonNull
    @Override
    public PerusahaanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_perusahaan, parent, false);
        return new PerusahaanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PerusahaanViewHolder holder, int position) {
        final Perusahaan currentPerusahaan = perusahaanList.get(position);

        holder.namaPerusahaanView.setText(currentPerusahaan.getNamaPerusahaan());
        holder.kategoriView.setText(currentPerusahaan.getKategori());

        holder.viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailIntent = new Intent(context, DetailPerusahaanActivity.class);
                detailIntent.putExtra("ID", currentPerusahaan.getId());
                context.startActivity(detailIntent);
            }
        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editIntent = new Intent(context, EditPerusahaanActivity.class);
                editIntent.putExtra("ID", currentPerusahaan.getId());
                context.startActivity(editIntent);
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase mDatabase = FirebaseDatabase.getInstance("https://gujob-papb-default-rtdb.asia-southeast1.firebasedatabase.app/");
                DatabaseReference mDBPerusahaanRef = mDatabase.getReference("Perusahaan").child(currentPerusahaan.getId());
                mDBPerusahaanRef.getRef().removeValue();
            }
        });
    }

    @Override
    public int getItemCount() {
        return perusahaanList.size();
    }

    public class PerusahaanViewHolder extends RecyclerView.ViewHolder {

        public Button viewButton, editButton, deleteButton;
        public TextView namaPerusahaanView, kategoriView;

        public PerusahaanViewHolder(@NonNull View itemView) {
            super(itemView);

            viewButton = itemView.findViewById(R.id.DetailButton);
            editButton = itemView.findViewById(R.id.AddButton);
            deleteButton = itemView.findViewById(R.id.DeleteButton);
            namaPerusahaanView = itemView.findViewById(R.id.NamaPerusahaanTextView);
            kategoriView = itemView.findViewById(R.id.KategoriTextView);
        }
    }
}
