package com.tugasakhirpapb.gujob.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tugasakhirpapb.gujob.DetailPerusahaanActivity;
import com.tugasakhirpapb.gujob.EditPerusahaanActivity;
import com.tugasakhirpapb.gujob.R;
import com.tugasakhirpapb.gujob.model.Lamaran;
import com.tugasakhirpapb.gujob.model.Perusahaan;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class LamaranAdapter extends RecyclerView.Adapter<LamaranAdapter.LamaranViewHolder> {

    private Context context;
    private List<Lamaran> lamaranList;

    public LamaranAdapter(Context context, ArrayList<Lamaran> lamaranList) {
        this.context = context;
        this.lamaranList = lamaranList;
    }

    @NonNull
    @Override
    public LamaranAdapter.LamaranViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lamaran, parent, false);
        return new LamaranAdapter.LamaranViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LamaranAdapter.LamaranViewHolder holder, int position) {
        final Lamaran currentLamaran = lamaranList.get(position);

        holder.namaView.setText(currentLamaran.getNama());
        holder.emailView.setText(currentLamaran.getEmail());

        holder.viewCvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseStorage.getInstance().getReference().child(currentLamaran.getCvUrl()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Intent fileIntent = new Intent(Intent.ACTION_VIEW, uri);
                        context.startActivity(fileIntent);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return lamaranList.size();
    }

    public class LamaranViewHolder extends RecyclerView.ViewHolder {

        public TextView namaView, emailView;
        public Button viewCvButton;

        public LamaranViewHolder(@NonNull View itemView) {
            super(itemView);

            namaView = itemView.findViewById(R.id.NamaLengkapTextView);
            emailView = itemView.findViewById(R.id.EmailTextView);
            viewCvButton = itemView.findViewById(R.id.ViewCvButton);
        }
    }
}
