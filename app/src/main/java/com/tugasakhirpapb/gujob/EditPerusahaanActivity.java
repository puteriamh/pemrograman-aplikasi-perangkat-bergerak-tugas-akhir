package com.tugasakhirpapb.gujob;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tugasakhirpapb.gujob.model.Perusahaan;

public class EditPerusahaanActivity extends AppCompatActivity {

    private EditText namaPerusahaanInput, kategoriInput, deskripsiInput;
    private Button editButton;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBPerusahaanRef;

    private Perusahaan perusahaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_perusahaan);
        Intent intent = getIntent();
        String perusahaanID = intent.getStringExtra("ID");

        namaPerusahaanInput = findViewById(R.id.NamaPerusahaanInput);
        kategoriInput = findViewById(R.id.KategoriInput);
        deskripsiInput = findViewById(R.id.DeskripsiInput);
        editButton = findViewById(R.id.EditButton);

        mDatabase = FirebaseDatabase.getInstance("https://gujob-papb-default-rtdb.asia-southeast1.firebasedatabase.app/");
        mDBPerusahaanRef = mDatabase.getReference("Perusahaan").child(perusahaanID);

        mDBPerusahaanRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                perusahaan = snapshot.getValue(Perusahaan.class);
                perusahaan.setId(snapshot.getKey());
                RefreshTampilan();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(EditPerusahaanActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditPerusahaan();
            }
        });
    }

    private void RefreshTampilan() {
        if (perusahaan == null) return;

        namaPerusahaanInput.setText(perusahaan.getNamaPerusahaan());
        kategoriInput.setText(perusahaan.getKategori());
        deskripsiInput.setText(perusahaan.getDeskripsi());
    }

    private void EditPerusahaan() {
        if (perusahaan == null) Toast.makeText(EditPerusahaanActivity.this, "Perusahaan Gagal Ditambahkan", Toast.LENGTH_SHORT).show();

        String namaPerusahaan = namaPerusahaanInput.getText().toString();
        String kategoriPerusahaan = kategoriInput.getText().toString();
        String deskripsiPerusahaan = deskripsiInput.getText().toString();

        if (namaPerusahaan == null) {
            Toast.makeText(EditPerusahaanActivity.this, "Nama Perusahaan Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (kategoriPerusahaan == null) {
            Toast.makeText(EditPerusahaanActivity.this, "Kategori Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (deskripsiPerusahaan == null) {
            Toast.makeText(EditPerusahaanActivity.this, "Deskripsi Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            Perusahaan newPerusahaan = new Perusahaan();
            newPerusahaan.setNamaPerusahaan(namaPerusahaan);
            newPerusahaan.setKategori(kategoriPerusahaan);
            newPerusahaan.setDeskripsi(deskripsiPerusahaan);

            mDBPerusahaanRef.setValue(newPerusahaan);

            Toast.makeText(EditPerusahaanActivity.this, "Perusahaan Berhasil Ditambahkan", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}